import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.Timer;


class stopwatchgui {	
	JFrame frmStopwatch;
    private JTextField outputHours;
    private JTextField outputMinutes;
    private JTextField outputSeconds;    
    private Timer timer;
    private JButton btnReset;
    DecimalFormat df = new DecimalFormat("0.##");
    private JTextField outputMilliseconds;
    private double EnableMilliseconds = 0;
    private Timer mstimer;
	int counter1;
	int counter2;
	int counter3;
	int counter4;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					stopwatchgui window = new stopwatchgui();
					window.frmStopwatch.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
    
	public stopwatchgui() {
		initialize();
		
	}
    void initialize() {
		frmStopwatch = new JFrame();
		frmStopwatch.setIconImage(Toolkit.getDefaultToolkit().getImage(timergui.class.getResource("logo.png")));
		frmStopwatch.setTitle("Stopwatch");
		frmStopwatch.setResizable(false);
		frmStopwatch.setBounds(100, 100, 245, 125);
		frmStopwatch.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmStopwatch.getContentPane().setLayout(null);
        
        outputHours = new JTextField();
        outputHours.setEditable(false);
        outputHours.setText("0");
        outputHours.setBounds(7, 23, 36, 20);
        frmStopwatch.getContentPane().add(outputHours);
        outputHours.setColumns(10);
        
        outputMinutes = new JTextField();
        outputMinutes.setEditable(false);
        outputMinutes.setText("0");
        outputMinutes.setBounds(51, 23, 36, 20);
        frmStopwatch.getContentPane().add(outputMinutes);
        outputMinutes.setColumns(10);
        
        outputSeconds = new JTextField();
        outputSeconds.setEditable(false);
        outputSeconds.setText("0");
        outputSeconds.setBounds(95, 23, 36, 20);
        frmStopwatch.getContentPane().add(outputSeconds);
        outputSeconds.setColumns(10);
        
        JLabel lblHours = new JLabel("Hours");
        lblHours.setBounds(7, 5, 55, 16);
        frmStopwatch.getContentPane().add(lblHours);
        
        JLabel lblMinutes = new JLabel("Mins");
        lblMinutes.setBounds(51, 5, 55, 16);
        frmStopwatch.getContentPane().add(lblMinutes);
        
        JLabel lblSeconds = new JLabel("Secs");
        lblSeconds.setBounds(95, 5, 55, 16);
        frmStopwatch.getContentPane().add(lblSeconds);
        
        JSeparator separatorButtons = new JSeparator();
        separatorButtons.setOrientation(SwingConstants.VERTICAL);
        separatorButtons.setBounds(142, 0, 2, 84);
        frmStopwatch.getContentPane().add(separatorButtons);
        
        final JLabel lblMilliseconds = new JLabel("Milliseconds");
        lblMilliseconds.setEnabled(false);
        lblMilliseconds.setBounds(7, 49, 80, 16);
        frmStopwatch.getContentPane().add(lblMilliseconds);
        
        outputMilliseconds = new JTextField();
        outputMilliseconds.setEnabled(false);
        outputMilliseconds.setEditable(false);
        outputMilliseconds.setText("0");
        outputMilliseconds.setBounds(95, 47, 36, 20);
        frmStopwatch.getContentPane().add(outputMilliseconds);
        outputMilliseconds.setColumns(10);
        
    	outputMilliseconds.setText(counter1 + "");
    	outputSeconds.setText(counter2 + "");
    	outputMinutes.setText(counter3 + "");
    	outputHours.setText(counter4 + "");
        
        
// Start Menu
        
        JMenuBar menuBar = new JMenuBar();
        frmStopwatch.setJMenuBar(menuBar);
        
        JMenu mnView = new JMenu("View");
        menuBar.add(mnView);
        
        JMenu mnOptions = new JMenu("Options");
        menuBar.add(mnOptions);
        
        JCheckBoxMenuItem chBtnEMS = new JCheckBoxMenuItem("Enable Milliseconds");
        chBtnEMS.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		if (EnableMilliseconds == 0){
	    	        lblMilliseconds.setEnabled(true);
	    	        outputMilliseconds.setEnabled(true);
        			EnableMilliseconds++;
        		}
        		
        		else if (EnableMilliseconds == 1){
	    	        lblMilliseconds.setEnabled(false);
	    	        outputMilliseconds.setEnabled(false);
        			EnableMilliseconds--;
        			}
        		}
        	});
        
        mnOptions.add(chBtnEMS);
        
		JMenuItem rdbtnmntmTimer = new JMenuItem("Menu");
		rdbtnmntmTimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gui menuWindow = new gui();
				menuWindow.frmTimingTools.setVisible(true);
				frmStopwatch.dispose();
			}
		});
		mnView.add(rdbtnmntmTimer);
		
		JMenuItem timerMenuButton = new JMenuItem("Timer");
		timerMenuButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				timergui timerWindow = new timergui();
				timerWindow.frmTimer.setVisible(true);
				frmStopwatch.dispose();
			}
		});
		mnView.add(timerMenuButton);
        
// End Menu
// Start Buttons
        
        JButton startButton = new JButton("Start");
        startButton.setBounds(149, 5, 86, 20);
        frmStopwatch.getContentPane().add(startButton);
        
        btnReset = new JButton("Reset");
        btnReset.setBounds(149, 47, 86, 20);
        frmStopwatch.getContentPane().add(btnReset);
        
        JButton btnPause = new JButton("Pause");
        btnPause.setBounds(149, 26, 86, 20);
        frmStopwatch.getContentPane().add(btnPause);        
        
        btnReset.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		resetCounting();
        	}
        });
        
        btnPause.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		timer.stop();
        		mstimer.stop();
        		updateCounting();
        	}
        });
                
        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
		    	if (EnableMilliseconds == 0)
		    		timer.start();
		    	
		    	if (EnableMilliseconds == 1) 
		    		mstimer.start();
            }
        });
        

// End Buttons
// Stopwatch Functions
        
        timer = new Timer(1000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	stopwatchCounter();
                
            }
        });
        
        mstimer = new Timer(57, new ActionListener() {
            public void actionPerformed(ActionEvent e) {    
            	stopwatchMsCounter();
            }
        });

    }        
    
    private void stopwatchCounter(){      
    		outputSeconds.setText(counter2++ + "");
    		outputMinutes.setText(counter3 + "");
    		
    		if (counter2 == 60){
    			counter2 = counter2 - 60;
        		outputMinutes.setText(counter3++ + "");
        		outputHours.setText(counter4 + "");
        		
    			if (counter3 == 60){
    				counter3 = counter3 - 60;
            		outputHours.setText(counter4++ + "");
    			}
    		}
    	}
    
    private void stopwatchMsCounter(){
        outputMilliseconds.setText((counter1 = counter1+57) + "");
		outputSeconds.setText(counter2 + "");
		
    	if (counter1 > 943){
    		counter1 = counter1 - 943;
    		outputSeconds.setText(counter2++ + "");
    		outputMinutes.setText(counter3 + "");
    		
    		if (counter2 == 60){
    			counter2 = counter2 - 60;
        		outputMinutes.setText(counter3++ + "");
        		outputHours.setText(counter4 + "");
        		
    			if (counter3 == 60){
    				counter3 = counter3 - 60;
            		outputHours.setText(counter4++ + "");
    			}
    		}	
    	}
    }
    
    private void updateCounting(){
    	outputMilliseconds.setText(counter1 + "");
    	outputSeconds.setText(counter2 + "");
    	outputMinutes.setText(counter3 + "");
    	outputHours.setText(counter4 + "");
    }
    
    private void resetCounting(){
    	timer.stop();
    	mstimer.stop();
    	outputMilliseconds.setText((counter1 = 0) + "");
    	outputSeconds.setText((counter2 = 0) + "");
    	outputMinutes.setText((counter3 = 0) + "");
    	outputHours.setText((counter4 = 0) + "");
    	
    }
}

