import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.JMenu;
import javax.swing.JTextField;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;

import java.awt.Toolkit;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.io.IOException;


class timergui {

	JFrame frmTimer;
	private JTextField txtHours;
	private JTextField txtMinutes;
	private JTextField txtSeconds;
	private double counter = 0;
	private int soundToggle = 1;
	private JTextField txtRemainingTime;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					timergui window = new timergui();
					window.frmTimer.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public timergui() {
		initialize();
		
	}
	void initialize() {
		frmTimer = new JFrame();
		frmTimer.setResizable(false);
		frmTimer.setIconImage(Toolkit.getDefaultToolkit().getImage(timergui.class.getResource("logo.png")));
		frmTimer.setTitle("Timer");
		frmTimer.setBounds(100, 100, 245, 125);
		frmTimer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTimer.getContentPane().setLayout(null);
        
        JLabel lblHours = new JLabel("Hours");
        lblHours.setBounds(7, 5, 55, 16);
        frmTimer.getContentPane().add(lblHours);
        
        JLabel lblMinutes = new JLabel("Mins");
        lblMinutes.setBounds(51, 5, 55, 16);
        frmTimer.getContentPane().add(lblMinutes);
        
        JLabel lblSeconds = new JLabel("Secs");
        lblSeconds.setBounds(94, 5, 55, 16);
        frmTimer.getContentPane().add(lblSeconds);
        
        JSeparator separatorButtons = new JSeparator();
        separatorButtons.setOrientation(SwingConstants.VERTICAL);
        separatorButtons.setBounds(142, 0, 2, 84);
        frmTimer.getContentPane().add(separatorButtons);
        
        JMenuBar menuBar = new JMenuBar();
        frmTimer.setJMenuBar(menuBar);
        

        
// Timer buttons
        
        JButton startButton = new JButton("Start");
        startButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		collectCounter();
        		timerTimer.start();
        	}
        });
        
        startButton.setBounds(149, 5, 86, 20);
        frmTimer.getContentPane().add(startButton);
        
        JButton btnReset = new JButton("Reset");
        btnReset.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		timerTimer.stop();
        		timerTimer.stop();
        		txtHours.setText("0");
        		txtMinutes.setText("0");
        		txtSeconds.setText("0");
        		txtRemainingTime.setText("Time Reset");
        		counter = 0;
        	}
        });
        
        btnReset.setBounds(149, 47, 86, 20);
        frmTimer.getContentPane().add(btnReset);
        
        JButton btnPause = new JButton("Pause");
        btnPause.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		timerTimer.stop();
        	}
        });
        
        btnPause.setBounds(149, 26, 86, 20);
        frmTimer.getContentPane().add(btnPause);
        
        txtHours = new JTextField();
        txtHours.setText("0");
        txtHours.setBounds(7, 26, 34, 20);
        frmTimer.getContentPane().add(txtHours);
        txtHours.setColumns(10);
        
        txtMinutes = new JTextField();
        txtMinutes.setText("0");
        txtMinutes.setBounds(48, 26, 34, 20);
        frmTimer.getContentPane().add(txtMinutes);
        txtMinutes.setColumns(10);
        
        txtSeconds = new JTextField();
        txtSeconds.setText("0");
        txtSeconds.setBounds(94, 26, 34, 20);
        frmTimer.getContentPane().add(txtSeconds);
        txtSeconds.setColumns(10);
        
        txtRemainingTime = new JTextField();
        txtRemainingTime.setEditable(false);
        txtRemainingTime.setText("Remaining Time");
        txtRemainingTime.setBounds(7, 47, 123, 20);
        frmTimer.getContentPane().add(txtRemainingTime);
        txtRemainingTime.setColumns(10);
    
// End Timer Buttons

// Menu
        
        JMenu mnView = new JMenu("View");
        menuBar.add(mnView);
        
        JMenuItem mntmMenu = new JMenuItem("Menu");
        mntmMenu.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
				gui window = new gui();
				window.frmTimingTools.setVisible(true);
				frmTimer.dispose();
        	}
        });
        
        mnView.add(mntmMenu);
        
		JMenuItem rdbtnmntmStopwatch = new JMenuItem("Stopwatch");
		rdbtnmntmStopwatch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stopwatchgui stopwatchWindow = new stopwatchgui();
				stopwatchWindow.frmStopwatch.setVisible(true);
				frmTimer.dispose();
			}
		});
        
		mnView.add(rdbtnmntmStopwatch);
        
        JMenu mnOptions = new JMenu("Options");
        menuBar.add(mnOptions);
        
        JCheckBoxMenuItem chckbxmntmPlaySound = new JCheckBoxMenuItem("Play sound");
        chckbxmntmPlaySound.setSelected(true);
        chckbxmntmPlaySound.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		playSoundToggle();
        	}
        });
        mnOptions.add(chckbxmntmPlaySound);
        
	}	
	
// End Menu

// Timer Functions
	
		Timer timerTimer = new Timer(1000, new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	            	timerStart();
	        }
		});

        private void timerStart(){
        	if (counter == 0){        	
        		playSoundCheck();
        	}
        		
        	if (counter != 0) {
        	counter--;
        	txtRemainingTime.setText("Time left: " + counter + "s");        	
        	}

        }
        
        private void playSoundCheck(){
        	while (soundToggle == 1){
       			playSound();
				
        		if (soundToggle == 1){
        			soundToggle = 0;
        			break;
        		}
        	}
        }
        
        private void playSoundToggle(){
			// Stops toggling code from looping
        	while (soundToggle != -1) {
        		if (soundToggle == 1){
        			soundToggle--;
        			break;
        		}
        	
        		if (soundToggle == 0){
        			soundToggle++;
        			break;
        		}
        	}
        }
        
        private void collectCounter(){
			// Only collect time input once
        	if (counter == 0)
        	counter = ((Double.parseDouble(txtHours.getText())*3600) + 
    				(Double.parseDouble(txtMinutes.getText())*60) + 
    				Double.parseDouble(txtSeconds.getText()));
        	
        }
        
        private void playSound(){
		
            	try {
            	         java.net.URL url = this.getClass().getClassLoader().getResource("Alarm.WAV");
            	         AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            	         Clip clip = AudioSystem.getClip();
            	         clip.open(audioIn);
            	         clip.start();
            	      } catch (UnsupportedAudioFileException e) {
            	         e.printStackTrace();
            	      } catch (IOException e) {
            	         e.printStackTrace();
            	      } catch (LineUnavailableException e) {
            	         e.printStackTrace();
            	      }
       }
}
