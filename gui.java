import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


class gui {

	JFrame frmTimingTools;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					gui window = new gui();
					window.frmTimingTools.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public gui() {
		GUI();
	}

	private void GUI() {
		frmTimingTools = new JFrame();
		frmTimingTools.setResizable(false);
		frmTimingTools.setTitle("Timing Tools");
		frmTimingTools.setBounds(100, 100, 245, 125);
		frmTimingTools.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmTimingTools.getContentPane().setLayout(null);
        
        final JLabel lblInstructions = new JLabel("Instructions");
        lblInstructions.setFont(new Font("Dialog", Font.BOLD, 14));
        lblInstructions.addMouseListener(new MouseAdapter() {
        	public void mouseEntered(MouseEvent arg0) {
                lblInstructions.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 14));
        	}
        	public void mouseExited(MouseEvent e) {
                lblInstructions.setFont(new Font("Dialog", Font.BOLD , 14));
        	}
        });
        lblInstructions.setToolTipText("Use the \"View\" menu tab to select the tool you want to use.");
        lblInstructions.setBounds(12, 12, 96, 27);
        frmTimingTools.getContentPane().add(lblInstructions);
        
        JLabel lblProgrammedByHarold = new JLabel("Programmed by Harold Seefeld");
        lblProgrammedByHarold.setBounds(12, 51, 194, 14);
        frmTimingTools.getContentPane().add(lblProgrammedByHarold);

		JMenuBar menuBar = new JMenuBar();
		frmTimingTools.setJMenuBar(menuBar);
		
		JMenu mnView = new JMenu("View");
		menuBar.add(mnView);
		
		JMenuItem rdbtnmntmTimer = new JMenuItem("Timer");
		rdbtnmntmTimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timergui timerWindow = new timergui();
				timerWindow.frmTimer.setVisible(true);
				frmTimingTools.dispose();
			}
		});
		mnView.add(rdbtnmntmTimer);
		
		JMenuItem rdbtnmntmStopwatch = new JMenuItem("Stopwatch");
		rdbtnmntmStopwatch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stopwatchgui stopwatchWindow = new stopwatchgui();
				stopwatchWindow.frmStopwatch.setVisible(true);
				frmTimingTools.dispose();
			}
		});
		mnView.add(rdbtnmntmStopwatch);
	}
}
